import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {setTimeout} from "timers/promises";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await setTimeout(1000);
  await app.listen(3000);
}
bootstrap();
