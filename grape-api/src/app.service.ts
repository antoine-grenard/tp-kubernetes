import { Injectable } from '@nestjs/common';
import { setTimeout } from "timers/promises";


const lastCallDates: Date[] = [new Date(Date.UTC(1900, 0, 1)), new Date(Date.UTC(1900, 0, 1))]

@Injectable()
export class AppService {

  getHello(): string {
    if(this.shouldSuicide()) {
      this.suicide()
    }

    lastCallDates.shift()
    lastCallDates.push(new Date())
    return 'Hello Grape! ';
  }

  shouldSuicide(): boolean {
    const dateMinus5Sec = new Date()
    dateMinus5Sec.setSeconds(dateMinus5Sec.getSeconds() - 10)

    let numberOfCallUnder5sec = 0

    if (lastCallDates[0] > dateMinus5Sec) {
      numberOfCallUnder5sec++
    }

    if (lastCallDates[1] > dateMinus5Sec) {
      numberOfCallUnder5sec++
    }

    console.log('should suicide : ', numberOfCallUnder5sec >= 2, lastCallDates)
    return numberOfCallUnder5sec >= 2
  }

  async suicide() {
    await setTimeout(100);
    console.log("bye.")
    process.exit(0);
  }

}
