import React, {useEffect, useState} from 'react';
import './App.css';
import axios from "axios";

export interface ApiStatus {
    isUp: boolean
}

export const App = () => {

    const [grape, setGrape] = useState<ApiStatus>()
    const [apple, setApple] = useState<ApiStatus>()
    const [banana, setBanana] = useState<ApiStatus>()
    const [orange, setOrange] = useState<ApiStatus>()
    const [watermelon, setWatermelon] = useState<ApiStatus>()

    useEffect(() => {
        checkApi()
    }, []);

    const checkApi = () => {
        axios.get('/grape-api/').then(() => { setGrape({isUp: true})},() => { setGrape({isUp: false})})
        axios.get('/apple-api/').then(() => { setApple({isUp: true})},() => { setApple({isUp: false})})
        axios.get('/banana-api/').then(() => { setBanana({isUp: true})},() => { setBanana({isUp: false})})
        axios.get('/orange-api/').then(() => { setOrange({isUp: true})},() => { setOrange({isUp: false})})
        axios.get('/watermelon-api/').then(() => { setWatermelon({isUp: true})},() => { setWatermelon({isUp: false})})
    }

    const getEmojiForStatus = (apiStatus?: ApiStatus): string => {
        if (apiStatus) {
            if (apiStatus.isUp) {
                return '🔥'
            } else {
                return '❌'
            }
        } else {
            return '⌛'
        }
    }
    return (
        <div className="App">
            <header className="App-header">
                <h1>
                    Fruit Salad
                </h1>
                <div className='fruitContainer'>
                    <img src='/empty_bowl.png' alt='empty_bowl'/>
                    <img src='/grape.png' alt='grape' hidden={!grape?.isUp}/>
                    <img src='/apple.png' alt='apple' hidden={!apple?.isUp}/>
                    <img src='/banana.png' alt='banana' hidden={!banana?.isUp}/>
                    <img src='/orange.png' alt='orange' hidden={!orange?.isUp}/>
                    <img src='/watermelon.png' alt='watermelon' hidden={!watermelon?.isUp}/>
                </div>
                <p>
                    Api Status <br/>
                    Apple Api : {getEmojiForStatus(apple)} <br/>
                    Banana Api : {getEmojiForStatus(banana)} <br/>
                    Grape Api : {getEmojiForStatus(grape)} <br/>
                    Orange Api : {getEmojiForStatus(orange)} <br/>
                    Watermelon Api : {getEmojiForStatus(watermelon)} <br/>
                </p>
            </header>
        </div>
    );
}
